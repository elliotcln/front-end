export { default as CarouselCard } from '../../components/CarouselCard.vue'
export { default as EditStoreMenu } from '../../components/EditStoreMenu.vue'
export { default as Footer } from '../../components/Footer.vue'
export { default as Header } from '../../components/Header.vue'
export { default as LoadMoreStores } from '../../components/LoadMoreStores.vue'
export { default as MainNav } from '../../components/MainNav.vue'
export { default as Modal } from '../../components/Modal.vue'
export { default as SearchBar } from '../../components/SearchBar.vue'
export { default as IconVerifiedStore } from '../../components/icon/IconVerifiedStore.vue'
export { default as AdminCreateMenu } from '../../components/admin/AdminCreateMenu.vue'
export { default as AdminFilesManager } from '../../components/admin/AdminFilesManager.vue'
export { default as AdminFilterStores } from '../../components/admin/AdminFilterStores.vue'
export { default as AdminGoBack } from '../../components/admin/AdminGoBack.vue'
export { default as AdminLoadMoreStores } from '../../components/admin/AdminLoadMoreStores.vue'
export { default as AdminNavbar } from '../../components/admin/AdminNavbar.vue'
export { default as AdminQrCodeGenerator } from '../../components/admin/AdminQrCodeGenerator.vue'
export { default as AdminStoreCard } from '../../components/admin/AdminStoreCard.vue'
export { default as AdminTagSelector } from '../../components/admin/AdminTagSelector.vue'
export { default as CreateStoreSteps } from '../../components/admin/CreateStoreSteps.vue'
export { default as GooglePlaces } from '../../components/admin/GooglePlaces.vue'
export { default as StoreImagesViewer } from '../../components/admin/StoreImagesViewer.vue'
export { default as Skeleton } from '../../components/skeleton/Skeleton.vue'
export { default as SkeletonStoreCard } from '../../components/skeleton/SkeletonStoreCard.vue'
export { default as StoreCard } from '../../components/card/StoreCard.vue'
export { default as MobileHead } from '../../components/mobile/MobileHead.vue'
export { default as MobileNav } from '../../components/mobile/MobileNav.vue'
export { default as MobileNavbar } from '../../components/mobile/MobileNavbar.vue'
export { default as FormUser } from '../../components/users/FormUser.vue'

export const LazyCarouselCard = import('../../components/CarouselCard.vue' /* webpackChunkName: "components/CarouselCard" */).then(c => c.default || c)
export const LazyEditStoreMenu = import('../../components/EditStoreMenu.vue' /* webpackChunkName: "components/EditStoreMenu" */).then(c => c.default || c)
export const LazyFooter = import('../../components/Footer.vue' /* webpackChunkName: "components/Footer" */).then(c => c.default || c)
export const LazyHeader = import('../../components/Header.vue' /* webpackChunkName: "components/Header" */).then(c => c.default || c)
export const LazyLoadMoreStores = import('../../components/LoadMoreStores.vue' /* webpackChunkName: "components/LoadMoreStores" */).then(c => c.default || c)
export const LazyMainNav = import('../../components/MainNav.vue' /* webpackChunkName: "components/MainNav" */).then(c => c.default || c)
export const LazyModal = import('../../components/Modal.vue' /* webpackChunkName: "components/Modal" */).then(c => c.default || c)
export const LazySearchBar = import('../../components/SearchBar.vue' /* webpackChunkName: "components/SearchBar" */).then(c => c.default || c)
export const LazyIconVerifiedStore = import('../../components/icon/IconVerifiedStore.vue' /* webpackChunkName: "components/icon/IconVerifiedStore" */).then(c => c.default || c)
export const LazyAdminCreateMenu = import('../../components/admin/AdminCreateMenu.vue' /* webpackChunkName: "components/admin/AdminCreateMenu" */).then(c => c.default || c)
export const LazyAdminFilesManager = import('../../components/admin/AdminFilesManager.vue' /* webpackChunkName: "components/admin/AdminFilesManager" */).then(c => c.default || c)
export const LazyAdminFilterStores = import('../../components/admin/AdminFilterStores.vue' /* webpackChunkName: "components/admin/AdminFilterStores" */).then(c => c.default || c)
export const LazyAdminGoBack = import('../../components/admin/AdminGoBack.vue' /* webpackChunkName: "components/admin/AdminGoBack" */).then(c => c.default || c)
export const LazyAdminLoadMoreStores = import('../../components/admin/AdminLoadMoreStores.vue' /* webpackChunkName: "components/admin/AdminLoadMoreStores" */).then(c => c.default || c)
export const LazyAdminNavbar = import('../../components/admin/AdminNavbar.vue' /* webpackChunkName: "components/admin/AdminNavbar" */).then(c => c.default || c)
export const LazyAdminQrCodeGenerator = import('../../components/admin/AdminQrCodeGenerator.vue' /* webpackChunkName: "components/admin/AdminQrCodeGenerator" */).then(c => c.default || c)
export const LazyAdminStoreCard = import('../../components/admin/AdminStoreCard.vue' /* webpackChunkName: "components/admin/AdminStoreCard" */).then(c => c.default || c)
export const LazyAdminTagSelector = import('../../components/admin/AdminTagSelector.vue' /* webpackChunkName: "components/admin/AdminTagSelector" */).then(c => c.default || c)
export const LazyCreateStoreSteps = import('../../components/admin/CreateStoreSteps.vue' /* webpackChunkName: "components/admin/CreateStoreSteps" */).then(c => c.default || c)
export const LazyGooglePlaces = import('../../components/admin/GooglePlaces.vue' /* webpackChunkName: "components/admin/GooglePlaces" */).then(c => c.default || c)
export const LazyStoreImagesViewer = import('../../components/admin/StoreImagesViewer.vue' /* webpackChunkName: "components/admin/StoreImagesViewer" */).then(c => c.default || c)
export const LazySkeleton = import('../../components/skeleton/Skeleton.vue' /* webpackChunkName: "components/skeleton/Skeleton" */).then(c => c.default || c)
export const LazySkeletonStoreCard = import('../../components/skeleton/SkeletonStoreCard.vue' /* webpackChunkName: "components/skeleton/SkeletonStoreCard" */).then(c => c.default || c)
export const LazyStoreCard = import('../../components/card/StoreCard.vue' /* webpackChunkName: "components/card/StoreCard" */).then(c => c.default || c)
export const LazyMobileHead = import('../../components/mobile/MobileHead.vue' /* webpackChunkName: "components/mobile/MobileHead" */).then(c => c.default || c)
export const LazyMobileNav = import('../../components/mobile/MobileNav.vue' /* webpackChunkName: "components/mobile/MobileNav" */).then(c => c.default || c)
export const LazyMobileNavbar = import('../../components/mobile/MobileNavbar.vue' /* webpackChunkName: "components/mobile/MobileNavbar" */).then(c => c.default || c)
export const LazyFormUser = import('../../components/users/FormUser.vue' /* webpackChunkName: "components/users/FormUser" */).then(c => c.default || c)
