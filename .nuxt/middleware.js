const middleware = {}

middleware['auth'] = require('../middleware/auth.js')
middleware['auth'] = middleware['auth'].default || middleware['auth']

middleware['check-stores'] = require('../middleware/check-stores.js')
middleware['check-stores'] = middleware['check-stores'].default || middleware['check-stores']

middleware['isAdmin'] = require('../middleware/isAdmin.js')
middleware['isAdmin'] = middleware['isAdmin'].default || middleware['isAdmin']

export default middleware
