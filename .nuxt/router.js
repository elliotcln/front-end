import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _2f5877c4 = () => interopDefault(import('../pages/admin.vue' /* webpackChunkName: "pages/admin" */))
const _3215c972 = () => interopDefault(import('../pages/admin/index.vue' /* webpackChunkName: "pages/admin/index" */))
const _468f8a62 = () => interopDefault(import('../pages/admin/modifications/index.vue' /* webpackChunkName: "pages/admin/modifications/index" */))
const _76bdfd61 = () => interopDefault(import('../pages/admin/profile/index.vue' /* webpackChunkName: "pages/admin/profile/index" */))
const _766658d0 = () => interopDefault(import('../pages/admin/stores/index.vue' /* webpackChunkName: "pages/admin/stores/index" */))
const _50d59937 = () => interopDefault(import('../pages/admin/tags/index.vue' /* webpackChunkName: "pages/admin/tags/index" */))
const _2ccaaa40 = () => interopDefault(import('../pages/admin/users/index.vue' /* webpackChunkName: "pages/admin/users/index" */))
const _28ceeac2 = () => interopDefault(import('../pages/admin/stores/add.vue' /* webpackChunkName: "pages/admin/stores/add" */))
const _44c2780f = () => interopDefault(import('../pages/admin/users/add.vue' /* webpackChunkName: "pages/admin/users/add" */))
const _30e7b431 = () => interopDefault(import('../pages/admin/stores/_id/edit.vue' /* webpackChunkName: "pages/admin/stores/_id/edit" */))
const _5508acc1 = () => interopDefault(import('../pages/admin/users/_id/edit.vue' /* webpackChunkName: "pages/admin/users/_id/edit" */))
const _0ad79f38 = () => interopDefault(import('../pages/admin/stores/_id/_old-edit.vue' /* webpackChunkName: "pages/admin/stores/_id/_old-edit" */))
const _876b9174 = () => interopDefault(import('../pages/cgu.vue' /* webpackChunkName: "pages/cgu" */))
const _197fe4d5 = () => interopDefault(import('../pages/contact.vue' /* webpackChunkName: "pages/contact" */))
const _6afa2e83 = () => interopDefault(import('../pages/search.vue' /* webpackChunkName: "pages/search" */))
const _4bbc86c6 = () => interopDefault(import('../pages/search/index.vue' /* webpackChunkName: "pages/search/index" */))
const _7e194b85 = () => interopDefault(import('../pages/search/tags/_tag.vue' /* webpackChunkName: "pages/search/tags/_tag" */))
const _cb9a72c6 = () => interopDefault(import('../pages/auth/login.vue' /* webpackChunkName: "pages/auth/login" */))
const _1768cb07 = () => interopDefault(import('../pages/auth/reset_password.vue' /* webpackChunkName: "pages/auth/reset_password" */))
const _e3545bc2 = () => interopDefault(import('../pages/stores/add.vue' /* webpackChunkName: "pages/stores/add" */))
const _6492a471 = () => interopDefault(import('../pages/stores/nearby.vue' /* webpackChunkName: "pages/stores/nearby" */))
const _6aa43f8b = () => interopDefault(import('../pages/stores/_slug/index.vue' /* webpackChunkName: "pages/stores/_slug/index" */))
const _2a9c4661 = () => interopDefault(import('../pages/stores/_slug/edit.vue' /* webpackChunkName: "pages/stores/_slug/edit" */))
const _042db872 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/admin",
    component: _2f5877c4,
    children: [{
      path: "",
      component: _3215c972,
      name: "admin"
    }, {
      path: "modifications",
      component: _468f8a62,
      name: "admin-modifications"
    }, {
      path: "profile",
      component: _76bdfd61,
      name: "admin-profile"
    }, {
      path: "stores",
      component: _766658d0,
      name: "admin-stores"
    }, {
      path: "tags",
      component: _50d59937,
      name: "admin-tags"
    }, {
      path: "users",
      component: _2ccaaa40,
      name: "admin-users"
    }, {
      path: "stores/add",
      component: _28ceeac2,
      name: "admin-stores-add"
    }, {
      path: "users/add",
      component: _44c2780f,
      name: "admin-users-add"
    }, {
      path: "stores/:id/edit",
      component: _30e7b431,
      name: "admin-stores-id-edit"
    }, {
      path: "users/:id/edit",
      component: _5508acc1,
      name: "admin-users-id-edit"
    }, {
      path: "stores/:id/:old-edit?",
      component: _0ad79f38,
      name: "admin-stores-id-old-edit"
    }]
  }, {
    path: "/cgu",
    component: _876b9174,
    name: "cgu"
  }, {
    path: "/contact",
    component: _197fe4d5,
    name: "contact"
  }, {
    path: "/search",
    component: _6afa2e83,
    children: [{
      path: "",
      component: _4bbc86c6,
      name: "search"
    }, {
      path: "tags/:tag?",
      component: _7e194b85,
      name: "search-tags-tag"
    }]
  }, {
    path: "/auth/login",
    component: _cb9a72c6,
    name: "auth-login"
  }, {
    path: "/auth/reset_password",
    component: _1768cb07,
    name: "auth-reset_password"
  }, {
    path: "/stores/add",
    component: _e3545bc2,
    name: "stores-add"
  }, {
    path: "/stores/nearby",
    component: _6492a471,
    name: "stores-nearby"
  }, {
    path: "/stores/:slug",
    component: _6aa43f8b,
    name: "stores-slug"
  }, {
    path: "/stores/:slug?/edit",
    component: _2a9c4661,
    name: "stores-slug-edit"
  }, {
    path: "/",
    component: _042db872,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
