export default function ({ app, redirect, route, store }) {
    // if(!store.state.user.isLoggedIn || store.state.user.token == null) {
    //     return redirect('/auth/login')
    // }
    if(app.$cookies.get('user') && app.$cookies.get('token')) {
        const user = app.$cookies.get('user')
        const token = app.$cookies.get('token')
        const payload = {
            user: user,
            token: token
        }
        store.commit('auth/LOGIN', payload)
    } else {
        if(route.name.includes('admin')) {
            return redirect('/auth/login')
        } else {
            return;
        }
    }
}