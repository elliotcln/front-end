export default function ({ app, next, store }) {
    if (!Object.values(store.state.stores.stores).length) {
        store.commit('stores/SET_LOADING', true)
        getStores(app, store, next)
    }
}

function getStores(app, store) {
    const headers = {
        'Authorization': 'Bearer ' + store.state.auth.token
    }
    app.$api.get('stores', {headers: headers}).then(response => {
        const stores = response.data;
        stores.data.forEach(s => {
            store.dispatch('stores/addStore', s)
        });
        store.commit('stores/SET_LOADING', false)
        store.dispatch('stores/setPagination', stores.meta)
    })
}