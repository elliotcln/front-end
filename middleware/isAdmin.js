export default function ({ redirect, store }) {
    if(!store.state.auth.isAdmin) {
        return redirect('/admin');
    } else {
        return;
    }
}