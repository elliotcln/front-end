export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Pretioù ~ Les cartes de vos restaurants préférés',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, viewport-fit=cover' },
      { hid: 'description', name: 'description', content: 'Toutes les cartes de vos établissements préférés à portée de clic.' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=BioRhyme:300,400,700|Work+Sans:400,700' },
    ]
  },
  ssr: false,
  watchQuery: true,
  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '@/assets/scss/main.scss'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/vue-unicons', mode: 'client' },
    '~/plugins/axios',
    '~/plugins/vue-gallery-slideshow',
    '~/plugins/fuse',
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/date-fns',
    '@nuxtjs/google-analytics'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/toast',
    'cookie-universal-nuxt',
  ],

  googleAnalytics: {
    id: 'UA-186833901-1',
    debug: {
      sendHitTask: true
    },
    autoTracking: {
      screenview: true
    }
  },

  toast: {
    theme: 'outline',
    position: 'bottom-center',
    duration: 2500,
  },

  proxy: [
    // 'http://pretiou-env.eba-dpqpdweg.eu-west-3.elasticbeanstalk.com'
    // 'https://onpropose.dev',
    // 'https://3d4755b8b159.ngrok.io/'
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    proxy: true
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  },

  pwa: {
    meta: {
      name: 'Pretioù',
      author: 'Elliot COLLIN <elliot.cln@gmail.com>',
      viewport: "width=device-width, initial-scale=1, viewport-fit=cover",
      lang: 'fr',
      appleStatusBarStyle: 'default',
      mobileAppIOS: true,
      theme_color: '#FFFFFF',
      nativeUI: true
    },
    manifest: {
      name: 'Pretioù',
      short_name: 'Pretioù',
      display: 'standalone',
      start_url: '/',
      ogDescription: 'Toutes les cartes de vos établissements préférés à portée de clic.',
      ogImage: {
        path: 'https://pretiou.fr/share@2x.png',
        width: "1200px",
        height: "628px",
        type: "image/png"
      }
    }
  }
}
