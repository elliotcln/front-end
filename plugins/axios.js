export default function ({ $axios }, inject) {
    // Create a custom axios instance
    const api = $axios.create({
        // baseURL: 'https://pretiou-api.dev/api/',
        baseURL: 'https://api.pretiou.fr/api/',
        // baseURL: 'http://pretiou-env.eba-hxwtpzeu.eu-west-3.elasticbeanstalk.com/api/',
        headers: {
            common: {
                'Accept': 'application/json, text/plain, */*',
                'X-Requested-With': 'XMLHttpRequest'
            },
        }
    })

    // Inject to context as $api
    inject('api', api)
}