import Vue from 'vue'
import Unicon from 'vue-unicons/dist/vue-unicons-ssr.common.js'
import { uniBars, uniMapMarkerAlt, uniTagAlt, uniStore, uniAngleLeft, 
    uniAngleRight, uniDashboard, uniHomeAlt, uniUsersAlt, uniSignout, uniTimes,
    uniHistory, uniPlus, uniCheck, uniPostcard, uniImages, uniFileAlt, uniTrash, uniEye, uniEdit, uniQrcodeScan, uniUser, uniSearch, uniSignInAlt, uniCompass, uniEllipsisH, uniClock, uniShoppingBag, uniRestaurant, uniInfoCircle, uniMapPinAlt, uniPhone, uniEnvelope, uniExternalLinkAlt, uniEditAlt, uniBell, uniMessage, uniEnvelopeHeart } from 'vue-unicons/src/icons'
import 'vue-unicons/dist/vue-unicons-ssr.css'

Unicon.add([uniBars, uniMapMarkerAlt, uniTagAlt, uniStore, uniAngleLeft, uniAngleRight, uniDashboard, uniHomeAlt,
    uniUsersAlt, uniSignout, uniSignInAlt, uniTimes, uniHistory, uniPlus, uniCheck, uniImages, uniPostcard, uniFileAlt, uniTrash, uniEye, uniEdit,
    uniQrcodeScan, uniUser, uniSearch, uniCompass, uniEllipsisH, uniClock, uniShoppingBag, uniRestaurant, uniInfoCircle, uniMapPinAlt,
uniPhone, uniEnvelope, uniExternalLinkAlt, uniEditAlt, uniBell, uniMessage, uniEnvelopeHeart])
Vue.use(Unicon, {
    fill: 'currentColor',
})
