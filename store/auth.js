export const state = () => ({
    user: null,
    token: null,
    loggedIn: false,
    isAdmin: false
})

export const mutations = {
    LOGIN(state, payload) {
        state.user = payload.user
        state.token = payload.token
        state.loggedIn = true
        if(payload.user.role === 2)
            state.isAdmin = true
    },
    LOGOUT(state) {
        state.user = null
        state.token = null
        state.loggedIn = false
        state.isAdmin = false
    }
}

export const actions = {
    logout({commit}) {
        commit('LOGOUT')
    }
}