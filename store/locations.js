export const state = () => ({
    coords: {
        lat: null,
        lng: null
    }
})

export const mutations = {
    SET_LAT(state, lat) {
        state.coords.lat = lat
    },
    SET_LNG(state, lng) {
        state.coords.lng = lng
    }
}