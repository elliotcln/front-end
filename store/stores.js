import Vue from 'vue'
export const state = () => ({
    storeLoading: false,
    storesPaginate: null,
    stores: {},
    userStores: {},
    nearbyStores: {},
    types: [
        'Restaurant',
        'Restaurant routier',
        'Restaurant gastronomique',
        'Brasserie',
        'Pizzeria',
        'Crêperie',
        'Bar',
        'Food-truck',
        'Snack-bar',
        'Bistrot'
    ],
    equipements: [
        'Accès handicapé',
        'Terrasse',
        'Jardin',
        'Parking privé',
        'Livraison à domicile',
        'Vente à emporter',
        'Salle de jeux'
    ]
})

export const mutations = {
    ADD_STORE: (state, store) => {
        Vue.set(state.stores, store.id, store)
    },
    ADD_NEARBY_STORE: (state, store) => {
        Vue.set(state.nearbyStores, store.id, store)
    },
    REMOVE_STORES: (state) => {
        state.stores = {}
    },
    UPDATE_STORE: (state, store) => {
        state.stores[store.id] = store
    },
    SET_PAGINATION(state, pagination) {
        state.storesPaginate = pagination
    },
    SET_LOADING(state, loading) {
        state.storeLoading = loading
    }
}

export const getters = {
    pagination: state => state.storesPaginate,
    stores: state => Object.values(state.stores),
    nearbyStores: state => state.nearbyStores,
    userStores: state => user_id => Object.values(state.stores).filter(s => s.user.id === user_id),
    storeById: state => id => Object.values(state.stores).filter(store => store.id === parseInt(id))[0],
    storeBySlug: state => slug => Object.values(state.stores).filter(store => store.infos.slug === slug)[0],
    publishedStores: state => Object.values(state.stores).filter(s => s.published === true)
}

export const actions = {
    addStore({ commit }, store) {
        commit('ADD_STORE', store)
    },
    editStore({ commit }, store) {
        commit('UPDATE_STORE', store)
    },
    setPagination({ commit }, pagination) {
        commit('SET_PAGINATION', pagination)
    }

}
