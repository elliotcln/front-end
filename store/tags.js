import Vue from 'vue'

export const state = () => ({
    tags: {}
})

export const mutations = {
    ADD_TAG(state, tag) {
        Vue.set(state.tags, tag.id, tag)
    },
    REMOVE_TAG(state, tag) {
        Vue.delete(state.tags, tag.id)
    }
}

export const getters = {
    tags: state => state.tags.sort()
}

export const actions = {
    addTag({commit}, tag) {
        commit('ADD_TAG', tag)
    },
    removeTag({commit}, tag) {
        commit('REMOVE_TAG', tag)
    }
}