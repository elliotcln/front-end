import Vue from 'vue'

export const state = () => ({
    users: {}
})

export const mutations = {
    ADD_USER: (state, user) => {
        Vue.set(state.users, user.id, user)
    },
    UPDATE_USER_ROLE(state, payload) {
        Vue.set(state.users[payload.id], 'role', Number(payload.role))
    },
    UPDATE_USER(state, user) {
        state.users[user.id] = user
    },
    REMOVE_USER(state, user) {
        Vue.delete(state.users, user.id)
    }
}

export const getters = {
    users: state => state.users,
    userById: state => id => Object.values(state.users).filter(user => user.id === parseInt(id))[0]
}

export const actions = {
    addUser({commit}, user) {
        commit('ADD_USER', user)
    },
    updateUserRole({commit}, payload) {
        commit('UPDATE_USER_ROLE', payload)
    },
    updateUser({commit}, user) {
        commit('UPDATE_USER', user)
    },
    deleteUser({commit}, user) {
        commit('REMOVE_USER', user)
    }
}