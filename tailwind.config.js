module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
    defaultLineHeights: true,
    standardFontWeights: true
  },
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        sans: [
          'Work Sans',
          'sans-serif'
        ],
        title: [
          'BioRhyme',
          'serif'
        ],
      },
      // colors
      colors: {
        dark: '#011D21',
        rich: '#13353B',
        munsell: '#1C8DA1',
        denim: {
          100: '#6CAAFC',
          200: '#4795FB',
          300: '#2280FA',
          400: '#056CF2',
          500: '#045CCF',
          600: '#0450B3',
          700: '#03449A',
          800: '#033980',
          900: '#022E66'
        },
        almond: {
          100: '#FDF6E4',
          200: '#FDF3DE',
          300: '#FDF1D7',
          400: '#FCEED1',
          500: '#FCECC9',
          600: '#F9D994',
          700: '#F6C65F',
          800: '#F3B329',
          900: '#D6970C'
        }
      }
    }
  },
  variants: {},
  plugins: []
}
